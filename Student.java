public class Student{
    private String student_name;
    private double r_score;
    private double class_r_score;
	private int amountLearnt;

	// Constructor
	public Student(String defaultName, double defaultRScore){
		this.student_name = defaultName;
		this.r_score = defaultRScore;
		this.class_r_score = 10;
		this.amountLearnt = 10;
	}

	// Instance Methods	
    public void rScoreCalc(){
        System.out.println(this.student_name + " has a r score of : " + this.r_score);
    }
	
	public void doHomework(){
		System.out.println(this.student_name + " is doing his homework.");
	}
	
	public void study(int amountStudied){
		amountLearnt += amountStudied;
	}
	
	// Setter
	public void setClassRScore(double classRScore){
		this.class_r_score = classRScore;
	}
	
	// Get methods
	public String getStudent_name(){
		return this.student_name;
	}
	public double getr_score(){
		return this.r_score;
	}
	public double getclass_r_score(){
		return this.class_r_score;
	}
	public int getamountLearnt(){
		return this.amountLearnt;
	}
}