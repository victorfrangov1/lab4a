import java.util.Scanner;
public class Application {
    public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		
        Student student1 = new Student("Victor", 100);
        Student student2 = new Student("Jeff", 100);	

        // student1.rScoreCalc();

        Student[] section4 = new Student[3];
        section4[0] = student1;
        section4[1] = student2;
        section4[2] = new Student("James", 100);

        // System.out.println(section4[0].getStudent_name());
		// System.out.println(section4[0].getr_score());
		// System.out.println(section4[0].getclass_r_score());
		// System.out.println(section4[0].getamountLearnt());
		
		System.out.println("How much have you studied ? ");
		int amountStudied = Integer.parseInt(reader.nextLine());
		section4[2].study(amountStudied);
		
		for(int i = 0; i < section4.length; i++){
			System.out.println(section4[i].getStudent_name() + " has learned " + section4[i].getamountLearnt());
		}
		
		Student studentTemp = new Student("Charles", 100);
		System.out.println(studentTemp.getStudent_name());
		System.out.println(studentTemp.getr_score());
		studentTemp.setClassRScore(30.00);
		System.out.println(studentTemp.getclass_r_score());
		System.out.println(studentTemp.getamountLearnt());
		

    }
}
